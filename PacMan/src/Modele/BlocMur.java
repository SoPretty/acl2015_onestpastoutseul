package Modele;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BlocMur extends Bloc{

	public BlocMur(int x, int y, Monde m) throws IOException{
		super(x,y,ImageIO.read(new File("Texture/Wall.png")),m);
	}
	
}
