package Modele;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;


public abstract class Bloc {

	private int x;
	private int y;
	private int hauteur;
	private int largeur;
	private BufferedImage texture;
	private Rectangle rect;

	public Bloc(int x,int y,BufferedImage texture, Monde m){
		this.hauteur = m.HauteurUnite;
		this.largeur = m.LargeurUnite;
		this.x = x*this.largeur;
		this.texture = texture;
		this.y = y*this.hauteur;
		rect = new Rectangle(this.x,this.y,largeur ,hauteur);
	}
	
	public Rectangle getRect() {
		return rect;
	}

	public void setRect(Rectangle rect) {
		this.rect = rect;
	}

	public BufferedImage getTexture() {
		return texture;
	}

	public int getPositionY() {
		return y;
	}

	public int getPositionX() {
		return x;
	}

}
