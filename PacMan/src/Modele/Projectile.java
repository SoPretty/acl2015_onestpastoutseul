package Modele;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Projectile extends Personnage {

	private Direction d;

	public Projectile(int x, int y,int rx, int ry,int sx, int sy, int deplacement, BufferedImage texture, Monde m, Direction d) throws IOException {
		super(x, y, 8, texture, m);
		this.sx = sx;
		this.sy = sy;
		this.rect = new Rectangle(rx, ry, this.sx, this.sy);
		this.d = d;
	}

	@Override
	public void update() {
		switch (d) {
		case STATIC:
			break;
		case LEFT:
			if (this.x - deplacement > 0) {
				if (!(this.getM().testerCollisionsPerso(this, this.x - deplacement, this.y))) {
					if (!this.getM().testerCollionsMur(this,this.x - deplacement, this.y)) {

						this.x = x - deplacement;
						rect.setLocation(this.x, this.y);
					}
				}
			}
			break;
		case RIGHT:
			if (this.x + deplacement + sx < this.getM().getLargeurLaby() - 10) {
				if (!(this.getM().testerCollisionsPerso(this, this.x + deplacement, this.y))) {
					if (!this.getM().testerCollionsMur(this,this.x + deplacement, this.y)) {
						this.x = x + deplacement;
						rect.setLocation(this.x, this.y);
					}
				}
			}
			break;
		case UP:
			if (this.y - deplacement > 0) {
				if (!(this.getM().testerCollisionsPerso(this, this.x, this.y - deplacement))) {
					if (!(this.getM().testerCollionsMur(this,this.x, this.y - deplacement))) {

						this.y = y - deplacement;
						rect.setLocation(this.x, this.y);
					}
				}
			}
			break;
		case DOWN:
			if (this.y + deplacement + sy < this.getM().getHauteurLaby() - 39) {
				if (!(this.getM().testerCollisionsPerso(this, this.x, this.y + deplacement))) {
					if (!this.getM().testerCollionsMur(this,this.x, this.y + deplacement)) {

						this.y = y + deplacement;
						rect.setLocation(this.x, this.y);
					}
					break;
				}
			}
		}
	}

	@Override
	public void update(Direction d) {
		// TODO Auto-generated method stub

	}

}