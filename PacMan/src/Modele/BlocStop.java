package Modele;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BlocStop extends Bloc{

	public BlocStop(int x, int y, Monde m) throws IOException{
		super(x,y,ImageIO.read(new File("Texture/link-NES.jpg")),m);
	}
	
}

