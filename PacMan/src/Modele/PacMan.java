package Modele;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import vue.VuePrincipale;

public class PacMan extends Personnage {

	boolean mort = false;

	public boolean isMort() {
		return mort;
	}

	public void setMort(boolean mort) {
		this.mort = mort;
	}

	public PacMan(Monde m) throws IOException {
		super(50, 50, 5, ImageIO.read(new File("Texture/LinkD.png")), m);
	}

	public void update(Direction d) throws IOException {
		this.d = d;

		switch (d) {
		case STATIC:
			if (this.getM().testerCollisionsPerso(this, this.x, this.y)) {
				mort = true;
			}
			break;
		case LEFT:
			this.setTexture(ImageIO.read(new File("Texture/LinkL.png")));
			if (this.x - deplacement > 0) {
				if (this.getM().testerCollionsTrap(this, this.x - deplacement,
						this.y)) {
					if (!this.trap) {
						this.x = x - deplacement;
						rect.setLocation(this.x, this.y);
						this.trap = true;
					}
				}
				if (!(this.getM().testerCollisionsPerso(this, this.x
						- deplacement, this.y))) {
					if (!this.getM().testerCollionsMur(this,
							this.x - deplacement, this.y)) {
						if (!this.trap) {
							this.x = x - deplacement;
							rect.setLocation(this.x, this.y);
						}
					}
				}
			}
			break;
		case RIGHT:
			this.setTexture(ImageIO.read(new File("Texture/LinkR.png")));
			if (this.x + deplacement + sx < this.getM().getLargeurLaby() - 10) {
				if (this.getM().testerCollionsTrap(this, this.x + deplacement,
						this.y)) {
					if (!this.trap) {
						this.x = x + deplacement;
						rect.setLocation(this.x, this.y);
						this.trap = true;
					}
				}
				if (!(this.getM().testerCollisionsPerso(this, this.x
						+ deplacement, this.y))) {
					if (!this.getM().testerCollionsMur(this,
							this.x + deplacement, this.y)) {
						if (!this.trap) {
							this.x = x + deplacement;
							rect.setLocation(this.x, this.y);
						}
					}
				}
			}
			break;
		case UP:
			this.setTexture(ImageIO.read(new File("Texture/LinkU.png")));
			if (this.y - deplacement > 0) {
				if ((this.getM().testerCollionsTrap(this, this.x, this.y
						- deplacement))) {
					if (!this.trap) {
						this.y = y - deplacement;
						rect.setLocation(this.x, this.y);
						this.trap = true;
					}
				}
				if (!(this.getM().testerCollisionsPerso(this, this.x, this.y
						- deplacement))) {
					if (!(this.getM().testerCollionsMur(this, this.x, this.y
							- deplacement))) {
						if (!this.trap) {
							this.y = y - deplacement;
							rect.setLocation(this.x, this.y);
						}
					}
				}
			}
			break;
		case DOWN:
			this.setTexture(ImageIO.read(new File("Texture/LinkD.png")));
			if (this.y + deplacement + sy < this.getM().getHauteurLaby() - 39) {
				if ((this.getM().testerCollionsTrap(this, this.x, this.y
						+ deplacement))) {
					if (!this.trap) {
						this.y = y + deplacement;
						rect.setLocation(this.x, this.y);
						this.trap = true;
					}
				}
				if (!(this.getM().testerCollisionsPerso(this, this.x, this.y
						+ deplacement))) {
					if (!this.getM().testerCollionsMur(this, this.x,
							this.y + deplacement)) {
						if (!this.trap) {
							this.y = y + deplacement;
							rect.setLocation(this.x, this.y);
						}
					}
					break;
				}
			}
		}
	}

}
