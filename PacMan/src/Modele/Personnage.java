package Modele;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

public abstract class Personnage {
	protected int x;
	 protected int y;
	 protected int sx;
	 protected int sy;
	 protected int deplacement;
	 protected Rectangle rect;
	 protected Direction d;
	protected BufferedImage texture;
	 private Monde m;
	 protected boolean trap = false;
	 
	


	public Personnage(int x,int y,int deplacement,BufferedImage texture,Monde m){
		this.m = m;
		this.x=x;
		this.y=y;
		this.deplacement = deplacement;
		this.sx = m.getHauteurUnite();
		this.sy = m.getLargeurUnite();
		this.texture = texture;
		rect = new Rectangle(x,y,sy ,sx);
	 }

	 
public Monde getM() {
		return m;
	}


	public void setM(Monde m) {
		this.m = m;
	}


public abstract void update(Direction d) throws IOException;


public int getDeplacement() {
	return deplacement;
}


public void setDeplacement(int deplacement) {
	this.deplacement = deplacement;
}


public Direction getD() {
	return d;
}


public void setD(Direction d) {
	this.d = d;
}


public void MisAJourRectangle(){
	rect.setLocation(this.x, this.y);
}

public int getX() {
	return x;
}


public void setX(int x) {
	this.x = x;
}


public int getY() {
	return y;
}

public BufferedImage getTexture() {
	return texture;
}


public void setTexture(BufferedImage texture) {
	this.texture = texture;
}


public void setY(int y) {
	this.y = y;
}

public Rectangle getRect() {
	return rect;
}


public void setRect(Rectangle rect) {
	this.rect = rect;
}


public int getSx() {
	return sx;
}


public void setSx(int sx) {
	this.sx = sx;
}


public int getSy() {
	return sy;
}


public void setSy(int sy) {
	this.sy = sy;
}


public void update() throws IOException {
	// TODO Auto-generated method stub
	
}
	
	 
	 
	
	
}
