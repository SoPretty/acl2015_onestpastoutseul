package Modele;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BlocSol extends Bloc{

	public BlocSol(int x, int y, Monde m) throws IOException{
		super(x,y,ImageIO.read(new File("Texture/sol.jpg")),m);
	}
	
}

