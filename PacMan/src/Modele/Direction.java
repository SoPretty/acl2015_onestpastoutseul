package Modele;
/**
 * Enumeration qui me permet de gerer les changements de directions.
 * @author Mathieu
 *
 */
public enum Direction {

	LEFT,RIGHT,UP,DOWN,STATIC;
	
}
