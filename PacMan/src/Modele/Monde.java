package Modele;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;

public class Monde {

	private PacMan pm;
	private ArrayList<Personnage> listPersonnage = new ArrayList<Personnage>();

	private ArrayList<Personnage> listMort = new ArrayList<Personnage>();

	private ArrayList<BlocMur> listMur = new ArrayList<BlocMur>();

	private ArrayList<BlocStop> listTrap = new ArrayList<BlocStop>();
	
	private ArrayList<BlocSol> listSol = new ArrayList<BlocSol>();

	// private ArrayList<Projectile> listProjectiles = new
	// ArrayList<Projectile>();

	// Hauteur d'une entit� dans le laby
	public static int HauteurUnite;
	// Largeur d'une entit� dans le laby
	public static int LargeurUnite;

	private int HauteurLaby;
	private int LargeurLaby;

	public Monde(int largeur, int hauteur) throws IOException {

		int Map[][] = { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1,1, 1, 1, 1, 1, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,1, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
				{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0,1, 1, 0, 0, 0, 1, 1, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 1, 0, 0, 0, 1, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 1, 0, 0, 0, 1, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,1, 1, 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

		this.LargeurLaby = largeur;
		this.HauteurLaby = hauteur;
		this.LargeurUnite = (this.LargeurLaby - 1) / Map[0].length;
		this.HauteurUnite = (this.HauteurLaby - 36) / Map.length;
		this.pm = new PacMan(this);
		Ennemi en = new Ennemi(250, 250,this);
		Ennemi en1 = new Ennemi(100, 289,this);
		this.listPersonnage.add(pm);
		this.listPersonnage.add(en);
		this.listPersonnage.add(en1);
		
		for (int i = 0; i < Map.length; i++) {
			for (int j = 0; j < Map[i].length; j++) {
				if (Map[i][j] == 1) {
					listMur.add(new BlocMur(j, i, this));
				} else if (Map[i][j] == 0) {
					listSol.add(new BlocSol(j, i, this));
				} else if(Map[i][j]== 2){
					listTrap.add(new BlocStop(j, i, this));
				}
			}
		}

	}

	public ArrayList<BlocSol> getListSol() {
		return listSol;
	}

	public void setListSol(ArrayList<BlocSol> listSol) {
		this.listSol = listSol;
	}

	public void personnageDeplacement(Direction d) throws IOException {
		// TODO Auto-generated method stub
		for (Personnage p : this.listPersonnage) {
			if (p instanceof PacMan) {
				p.update(d);
			} else {
				p.update();
			}
		}
	}

	public boolean testerCollionsMur(Personnage p ,int newX, int newY) {
		boolean move = false;
		// TODO Auto-generated method stub
		Rectangle r;
		for (BlocMur bm : this.listMur) {
			
			if(p instanceof Projectile){
				
				r = new Rectangle(newX, newY, p.getSx(),p.getSy());
				if (bm.getRect().getBounds().intersects(r.getBounds())) {
					return true;
				}
				
			}else{
				
			// test si le prochain mouvement est r�alisable ou non
			r = new Rectangle(newX, newY, this.LargeurUnite, this.HauteurUnite);
			if (bm.getRect().getBounds().intersects(r.getBounds())) {
				return true;
			}

		}
		}
		return move;
	}
	
	public boolean testerCollisionsProjectile(Personnage p, int newX, int newY){
		Rectangle r;
		for(Personnage p2 : listPersonnage){
			if(p2 instanceof Projectile && p instanceof Ennemi){
				r = new Rectangle(newX, newY, this.LargeurUnite, this.HauteurUnite);
				if (!this.listMort.contains(p2)) {
					
				if (p2.getRect().getBounds().intersects(r.getBounds())) {
					this.listMort.add(p2);
					this.listMort.add(p);
			}
				}
			}
		}
		
		return true;
	}

	public boolean testerCollionsTrap(Personnage p ,int newX, int newY) {
		boolean move = false;
		// TODO Auto-generated method stub
		Rectangle r;
		for (BlocStop bs : this.listTrap) {
			// test si le prochain mouvement est r�alisable ou non
			r = new Rectangle(newX, newY, this.LargeurUnite, this.HauteurUnite);
			if (bs.getRect().getX()< p.getRect().getX() +10 && 
					bs.getRect().getX()> p.getRect().getX() -10 &&
					bs.getRect().getY() < p.getRect().getY() +10 &&
					bs.getRect().getY()> p.getRect().getY() -10) {
				return true;
			}
		}
		return move;
	}
	
	public boolean testerCollisionsPerso(Personnage p, int newX, int newY) {
		boolean move = false;
		// TODO Auto-generated method stub
		Rectangle r;
		for (Personnage p2 : this.listPersonnage) {
			if(!(p2 instanceof Projectile) && (!(p instanceof Projectile))){
			if (p != p2) {
				// test si le prochain mouvement est r�alisable ou non
				r = new Rectangle(newX, newY, this.LargeurUnite, this.HauteurUnite);
				if (!this.listMort.contains(p2)) {
						if (p2.getRect().getBounds().intersects(r.getBounds())) {
							this.listMort.add(p);
							return true;
						}
				}

				}
			}
		}
		return move;

	}

	public ArrayList<Personnage> getListPersonnage() {
		return listPersonnage;
	}

	public void setListPersonnage(ArrayList<Personnage> listPersonnage) {
		this.listPersonnage = listPersonnage;
	}

	public ArrayList<Personnage> getListMort() {
		return listMort;
	}

	public void setListMort(ArrayList<Personnage> listMort) {
		this.listMort = listMort;
	}

	public int getHauteurLaby() {
		return HauteurLaby;
	}

	public void setHauteurLaby(int hauteurLaby) {
		HauteurLaby = hauteurLaby;
	}

	public int getLargeurLaby() {
		return LargeurLaby;
	}

	public void setLargeurLaby(int largeurLaby) {
		LargeurLaby = largeurLaby;
	}

	public int getHauteurUnite() {
		return HauteurUnite;
	}

	public void setHauteurUnite(int hauteurUnite) {
		HauteurUnite = hauteurUnite;
	}

	public int getLargeurUnite() {
		return LargeurUnite;
	}

	public void setLargeurUnite(int largeurUnite) {
		LargeurUnite = largeurUnite;
	}

	public ArrayList<BlocMur> getListMur() {
		return listMur;
	}

	public void setListMur(ArrayList<BlocMur> listMur) {
		this.listMur = listMur;
	}

	public int getPacmanPX() {
		return this.pm.getX();
	}

	public int getPacmanPY() {
		return this.pm.getY();
	}

	public void setPacmanPX(int x) {
		this.pm.setX(x);
	}

	public void setPacmanPY(int x) {
		this.pm.setY(x);
	}

	public PacMan getPm() {
		return pm;
	}

	public void setPm(PacMan pm) {
		this.pm = pm;
	}

	public ArrayList<Personnage> getPersonnages() {
		return listPersonnage;
	}

	public void setPersonnages(ArrayList<Personnage> personnages) {
		this.listPersonnage = personnages;
	}

	/*
	 * public ArrayList<Projectile> getListProjectiles() { return
	 * listProjectiles; }
	 * 
	 * 
	 * public void setListProjectiles(ArrayList<Projectile> listProjectiles) {
	 * this.listProjectiles = listProjectiles; }
	 */
}
