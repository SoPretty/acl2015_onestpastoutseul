package Modele;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import vue.VuePrincipale;

public class Ennemi extends Personnage {

	public Ennemi(int X, int Y, Monde m) throws IOException {
		super(X, Y, 2, ImageIO.read(new File("Texture/MonsterD.png")), m);
	}

	@Override
	public void update() throws IOException {
		if (!(this.getM().getListMort().contains(this))) {
			int r = 1 + (int) (Math.random() * ((4 - 1) + 1));

			int dx = this.getM().getPm().getX() - this.x;
			int dy = this.getM().getPm().getY() - this.y;
			r = 1 + (int) (Math.random() * ((2 - 1) + 1));
			int dir = 0;
			if (dx == 0 && dy < 0) {
				dir = 2;
			}
			if (dx == 0 && dy > 0) {
				dir = 4;
			}
			if (dx < 0 && dy == 0) {
				dir = 1;
			}
			if (dx > 0 && dy == 0) {
				dir = 3;
			}
			if (dx < 0 && dy < 0) {
				if (r == 1) {
					dir = 1;
				} else {
					dir = 2;
				}

			} else if (dx < 0 && dy > 0) {
				if (r == 1) {
					dir = 1;
				} else {
					dir = 4;
				}

			} else if (dx > 0 && dy < 0) {

				if (r == 1) {
					dir = 3;
				} else {
					dir = 2;
				}
			} else if (dx > 0 && dy > 0) {
				if (r == 1) {
					dir = 3;
				} else {
					dir = 4;
				}

			}

			switch (dir) {

			case 1:
				this.setTexture(ImageIO.read(new File("Texture/MonsterL.png")));
				this.d = Direction.LEFT;
				if (this.x - deplacement > 0) {
					if (!this.getM().testerCollionsMur(this,
							this.x - deplacement, this.y)) {
						if (!this.getM().testerCollionsTrap(this,
								this.x - deplacement, this.y)) {
							if (!this.trap) {
								this.x = x - deplacement;
								rect.setLocation(this.x, this.y);
							}
						}else if (!this.trap) {
							if (!this.trap) {
								this.trap = true;
								this.x = x - deplacement;
								rect.setLocation(this.x, this.y);

							}
						}
					}
				}
				this.getM().testerCollisionsProjectile(this,
						this.x - deplacement, this.y);

				break;
			case 3:
				this.setTexture(ImageIO.read(new File("Texture/MonsterR.png")));
				this.d = Direction.RIGHT;
				if (this.x + deplacement + sx < this.getM().getLargeurLaby() - 10) {
					if (!this.getM().testerCollionsMur(this,
							this.x + deplacement, this.y)) {

						if (!this.getM().testerCollionsTrap(this,
								this.x + deplacement, this.y)) {
							if (!this.trap) {
								this.x = x + deplacement;
								rect.setLocation(this.x, this.y);
							}
						}else if (!this.trap) {
							if (!this.trap) {
								this.trap = true;
								this.x = x + deplacement;
								rect.setLocation(this.x, this.y);

							}
						}
					}
				}

				this.getM().testerCollisionsProjectile(this,
						this.x + deplacement, this.y);
				break;
			case 2:
				this.setTexture(ImageIO.read(new File("Texture/MonsterU.png")));
				this.d = Direction.UP;
				if (this.y - deplacement > 0) {
					if (!this.getM().testerCollionsMur(this, this.x,
							this.y - deplacement)) {
						if (!this.getM().testerCollionsTrap(this, this.x,
								this.y - deplacement)) {
							this.y = y - deplacement;
							rect.setLocation(this.x, this.y);
						} else if (!this.trap) {
							if (!this.trap) {
								this.trap = true;
								this.y = y - deplacement;
								rect.setLocation(this.x, this.y);

							}
						}
					}
				}
				this.getM().testerCollisionsProjectile(this, this.x,
						this.y - deplacement);

				break;
			case 4:
				this.setTexture(ImageIO.read(new File("Texture/MonsterD.png")));
				this.d = Direction.DOWN;
				if (this.y + deplacement + sy < this.getM().getHauteurLaby() - 39) {
					if (!this.getM().testerCollionsMur(this, this.x,
							this.y + deplacement)) {
						if (!this.getM().testerCollionsTrap(this, this.x,
								this.y + deplacement)) {
							if (!this.trap) {
								this.y = y + deplacement;
								rect.setLocation(this.x, this.y);
							}
						}else if (!this.trap) {
							if (!this.trap) {
								this.trap = true;
								this.y = y + deplacement;
								rect.setLocation(this.x, this.y);

							}
						}
					}
					this.getM().testerCollisionsProjectile(this, this.x,
							this.y + deplacement);

					break;
				}
			}
		}
	}

	@Override
	public void update(Direction d) {
		// TODO Auto-generated method stub

	}

}
