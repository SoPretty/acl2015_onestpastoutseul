package Controller;

import java.awt.event.KeyListener;

import Modele.Direction;

/**
 * @author Horatiu Cirstea
 * 
 * controleur qui envoie des commandes au jeu 
 * 
 */
public interface IController extends KeyListener {

	public Direction getDirection();

}
