package Controller;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import Modele.Direction;
import Modele.Monde;
import Modele.Projectile;

public class PacmanController implements IController{
	
	private Direction d = Direction.STATIC;
	
	private Monde m;
	
	private Direction lastDirection = Direction.DOWN; 
	
	public PacmanController(Monde m){
		this.m = m;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			this.d = Direction.UP;
			this.lastDirection = Direction.UP;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN){
			this.d = Direction.DOWN;
			this.lastDirection = Direction.DOWN;
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT){
			this.d = Direction.LEFT;
			this.lastDirection = Direction.LEFT;
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			this.d = Direction.RIGHT;
			this.lastDirection = Direction.RIGHT;
		}
		if(e.getKeyCode() == KeyEvent.VK_SPACE){
			try {
				if(!m.getListMort().contains(m.getPm())){
				switch(lastDirection){
				
				case DOWN: 
					
					this.m.getListPersonnage().add(new Projectile(m.getPacmanPX()+(m.LargeurUnite/2),m.getPacmanPY()+30,4,19,4,19,8,ImageIO.read(new File("Texture/FlecheD.png")),m,this.lastDirection));

					
					break; 
					
				case UP:
					
					this.m.getListPersonnage().add(new Projectile(m.getPacmanPX()+(m.LargeurUnite/2),m.getPacmanPY()+m.HauteurUnite-50,m.getPacmanPX(),m.getPacmanPY(),4,19,8,ImageIO.read(new File("Texture/FlecheU.png")),m,this.lastDirection));

					
					break;

				case RIGHT:

					this.m.getListPersonnage().add(new Projectile(m.getPacmanPX()+m.LargeurUnite+10,m.getPacmanPY()+m.HauteurUnite/2,m.getPacmanPX()+4,m.getPacmanPY(),19,4,8,ImageIO.read(new File("Texture/FlecheR.png")),m,this.lastDirection));

					
					break;
				case LEFT:
					
					this.m.getListPersonnage().add(new Projectile(m.getPacmanPX()-50,m.getPacmanPY()+m.HauteurUnite/2,m.getPacmanPX(),m.getPacmanPY(),19,4,8,ImageIO.read(new File("Texture/FlecheG.png")),m,this.lastDirection));

					break;
				}
				}
				
				
				
				
				
				
		
			
			
			
			
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		this.d = Direction.STATIC;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public Direction getDirection() {
		// TODO Auto-generated method stub
		return this.d;
	}
	
}