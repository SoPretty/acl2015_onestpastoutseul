import java.io.IOException;

import Controller.PacmanController;
import Modele.Direction;
import Modele.Monde;
import vue.VuePrincipale;




public class Main  {
	
	public static void main(String[] args) throws InterruptedException, IOException {

		int Hauteur = 400;
		int Largeur = 600;
		Monde m = new Monde(Largeur,Hauteur);
		PacmanController pc = new PacmanController(m);
		VuePrincipale v = new VuePrincipale(m,pc);
		Direction d;
		

		while(true){
			d = pc.getDirection();
			m.personnageDeplacement(d);
			if(m.getPm().isMort()){
				m.getListMort().add(m.getPm());
			}
			v.repaint();
			Thread.sleep(20);
		}
	}

}
