package vue;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import Modele.BlocMur;
import Modele.BlocSol;
import Modele.Monde;
import Modele.Personnage;
import Modele.Projectile;

public class GameVue extends JPanel {

	private Monde m;
	private ArrayList<BlocMur> bm;
	private ArrayList<BlocSol> bs;

	public GameVue(Monde m) {
		this.m = m;
		bm = this.m.getListMur();
		bs = this.m.getListSol();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (BlocMur b : bm) {
			g.drawImage(b.getTexture(), b.getPositionX(), b.getPositionY(), m.LargeurUnite, m.HauteurUnite, null);
			g.drawRect(b.getPositionX(), b.getPositionY(), m.LargeurUnite, m.HauteurUnite);
			
		}
		for (BlocSol b : bs) {
			
			g.drawImage(b.getTexture(), b.getPositionX(), b.getPositionY(), m.LargeurUnite, m.HauteurUnite, null);
		}

		for (Personnage p : this.m.getPersonnages()) {
			
			
			if (!(m.getListMort().contains(p))) {
				if(p instanceof Projectile){
				g.drawImage(p.getTexture(), p.getX(), p.getY(), p.getSx(), p.getSy(), null);
				}else{
				g.drawImage(p.getTexture(), p.getX(), p.getY(), m.LargeurUnite, m.HauteurUnite, null);
				}
				}
			
		}

	}

}
