package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import Controller.PacmanController;
import Modele.Monde;


public class VuePrincipale extends JFrame{

	private Monde monde;
	private GameVue panel;
	
	
	public VuePrincipale(Monde m, PacmanController pc) throws InterruptedException{
		super("Projet ACL - PACMAN");
		this.monde = m;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(m.getLargeurLaby(), m.getHauteurLaby())) ;
		//Ajout du listener pour d�placer PacMan
		this.addKeyListener(pc);	
		//affichage des murs
		//Cr�ation de PacMan ( cercle bleu )
		panel = new GameVue(m); 
		this.setLayout(new BorderLayout()); 
		this.add(panel); 
		this.pack();
		setVisible(true);
	}

	public Monde getMonde() {
		return monde;
	}

	public void setMonde(Monde m) {
		this.monde = m;
	}

	public void repaint(){
		panel.repaint();
	}
}



