# README #

Dans BitBucket : 
Misternutz = Leriche Pierre
SoPretty = Jeanmougin Mathieu


Sprint v1.1 = dernière version pour le rendu du 18/11/2015

Pour cette version, nous avons ajouté une interface graphique avec un cercle (PACMAN). 
Les flèches permettent de bouger PacMan dans le labyrinthe.

Pour compiler notre projet : javac */*.java
Pour compiler les test : javac -cp <jar junit> *.java

Pour le lancer : java Main

Sprint v2.0 = version fonctionnelle pour le rendu du 19/11/2015

Pour cette version, nous avons gardé la version fonctionnelle précédente ( format MVC ) + 2/3 corrections.
Ainsi qu'un diagramme de classe (format PNJ/ ObjectAid) ainsi qu'un diagramme de séquences (Déplacement vers le bas).
Le travail effectué le 19/11/2015, matin, n'est pas présent car il n'est pas encore fonctionnel, il compte l'être pour le 25/11/2015.
On a/aura : 
    - Ajouté un système de Case Vide/Mur.
    - Enlevé le format MVC pour un format plus adapté à notre projet.
    - Une gestion des collisions.

Sprint v2.1 = version fonctionnelle pour le rendu du 02/12/2015

Pour cette version, nous avons revu la presque la totalité de notre projet ( passer du MVC à une boucle infinie ) ! Nous avons :
   
   - Modifié le contrôleur, la vue et le modèle pour que notre nouvelle architecture fonctionne comme avec le modèle MVC.
   - Modifié certains tests pour s'adapter à la nouvelle architecture.
   - Ajouté une texture pour le labyrinthe.
   - Ajouté un système de vitesse pour que PacMan bouge plus rapidement.
   - Un système de déplacement et de gestion de collision adapté à l'architecture.
  
L'objectif pour le prochain sprint est de :

   - Compléter les tests.
   - Ajouter une texture pour PacMan.
   - Créer des blocs "sol" ainsi que lui ajouter des textures.
   - Epurer le code.

Sprint v3.0 = version fonctionnelle pour le rendu du 03/12/2015

Pour cette version nous avons : 
 - Ajouté un ennemi 
 - Ajouté un mouvement aléatoire sur cet ennemi
 - Ajouté un nouveau diagramme de séquence / classe
 - Ajouté des textures 
 - Effectué des test

Sprint v4.0 = version fonctionnelle pour le rendu final du 17/12/2015
* Création des projectiles
* Gestion des collisions avec les projectiles
* Ajout de nouveau tests liés aux projectiles
* Ajout d'un nouveau diagramme de classe